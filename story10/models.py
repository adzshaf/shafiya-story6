from django.db import models

# Create your models here.


class SubscriberModels(models.Model):
    name = models.CharField(max_length=100)
    email = models.EmailField(unique=True, db_index=True)
    password = models.CharField(max_length=15)
