from django.test import TestCase
from django.test import Client
from django.urls import resolve

from . import views


class SampleTest(TestCase):
    # test eksistensi landing page
    def test_homepage(self):
        response = Client().get('/subscribers/')
        self.assertEqual(response.status_code, 200)

    def test_home_using_index_func(self):
        found = resolve('/subscribers/')
        self.assertEqual(found.func, views.subscribe)

    def test_status_template_used(self):
        response = Client().get('/subscribers/')
        self.assertTemplateUsed(response, 'page/story10.html')
