from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^subscribers/$', views.subscribe, name='subscribe'),
    url(r'^save_subscribers/$', views.save_subscribers, name='save_subscribers'),
    url(r'^show_subscribers/$', views.show_subscribers, name='show_subscribers'),
    url(r'^delete_subscribers/$', views.delete_subscribers, name='delete_subscribers')
]
