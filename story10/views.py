from django.shortcuts import render
import requests
from django.core import serializers
from django.http import HttpResponse
from django.db import IntegrityError
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
# Create your views here.

from .forms import SubscriberForms
from .models import SubscriberModels

response = {}


def subscribe(request):
    form = SubscriberForms(request.POST)
    html = 'page/story10.html'
    return render(request, html, {'form': form})


def save_subscribers(request):
    response['form'] = SubscriberForms()
    form = SubscriberForms(request.POST or None)
    if request.method == 'POST' and form.is_valid():
        response['name'] = request.POST['name']
        response['email'] = request.POST['email']
        response['password'] = request.POST['password']
        status = 'True'
        # Store account into subscribers database
        try:
            storedata = SubscriberModels(
                name=response['name'],
                email=response['email'],
                password=response['password']
            )
            storedata.save()
            return JsonResponse({'status': status})
        except IntegrityError as e:
            status = 'False'
            return JsonResponse({'status': status})
    status = 'False'
    return JsonResponse({'status': status})


def show_subscribers(request):
    data = list(SubscriberModels.objects.all().values(
        'pk', 'name', 'email', 'password'))
    return JsonResponse({'subsdata': data})


@csrf_exempt
def delete_subscribers(request):
    if(request.method == 'POST'):
        subsdata = request.POST['pk']
        SubscriberModels.objects.filter(pk=subsdata).delete()
        return HttpResponse({'status': 'Success'})
    else:
        return HttpResponse({'status': 'Failed to load page'})
