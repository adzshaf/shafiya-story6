from django.test import TestCase
from django.test import Client
from django.urls import resolve

from . import views

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time


class SampleTest(TestCase):
    # test eksistensi landing page
    def test_homepage(self):
        response = Client().get('/list/')
        self.assertEqual(response.status_code, 200)

    def test_home_using_index_func(self):
        found = resolve('/list/')
        self.assertEqual(found.func, views.list)

    def test_status_template_used(self):
        response = Client().get('/list/')
        self.assertTemplateUsed(response, 'page/story9.html')

    def test_json_reply(self):
        json = '''[{"id": "156613", "name": "Lenovo Thinkpad Edge E480-55ID"'''
        response = Client().get("/list-json/")
        self.assertIn(json, response.content.decode())


class FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome(
            './chromedriver', chrome_options=chrome_options)
        self.selenium.get('http://localhost:8000/list/')

    def tearDown(self):
        self.selenium.quit()

    def test_opening_a_browser(self):
        selenium = self.selenium
        self.assertIn('Shafiya\'s Story 9', selenium.title)

    def test_klik_add_to_wishlist(self):
        selenium = self.selenium
        time.sleep(10)
        button = selenium.find_element_by_id('156613')
        button.click()
        price = selenium.find_element_by_xpath("//*[@id='buy']")
        self.assertIn("15300000", price.text)

    def test_klik_remove_from_wishlist(self):
        selenium = self.selenium
        time.sleep(10)
        button = selenium.find_element_by_id('156613')
        button.click()
        button.click()
        price = selenium.find_element_by_xpath("//*[@id='buy']")
        self.assertIn("0", price.text)
