from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^list/$', views.list, name='list'),
    url(r'^list-json/$', views.list_json, name='list_json')
]
