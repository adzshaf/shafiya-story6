$(document).ready(function() {
  let currentbg = $("body").css("background-color");
  let currentText = $("h1").css("color");

  let expectbg = "black";
  let expectText = "black";

  $(".accordion").on("click", ".accordion-header", function() {
    $(this)
      .toggleClass("active")
      .next()
      .slideToggle();
  });

  $("#switch").mouseenter(function() {
    $("body").css("background-color", expectbg);
    $("body").css("transition", "0.5s");
    $("h1").css("color", expectText);
  });

  $("#switch").mouseleave(function() {
    $("body").css("background-color", currentbg);
    $("body").css("transition", "0.5s");
    $("h1").css("color", currentText);
  });

  $("#switch").click(function() {
    $("body").css("background-color", expectbg);
    $("body").css("transition", "0.5s");
    $("h1").css("color", expectText);
    if (expectbg == "black") {
      expectbg = "rgb(227, 95, 117)";
      expectText = "rgb(64, 78, 136)";
      currentbg = "black";
      currentText = "black";
    } else {
      expectbg = "black";
      expectText = "black";
      currentbg = "rgb(227, 95, 117)";
      currentText = "rgb(64, 78, 136)";
    }
  });
});
