$(document).ready(function() {
  $.ajax({
    url: "/list-json/",
    type: "GET",
    dataType: "json",
    success: function(result) {
      let header =
        "<tr><th>No</th><th>Name</th><th>Details</th><th>Price</th><th>Wishlist</th></tr><div id='row'></div>";
      $("#tabel")
        .append(header)
        .attr("border", "1px ");
      getall(result);
      harga();
    }
  });
});

function getall(data) {
  let final_data = data["data"];
  for (var i = 0; i < 100; ++i) {
    var row =
      "<tr>\
        <td>" +
      (i + 1) +
      "</td><td>" +
      final_data[i].name +
      "</td><td>" +
      final_data[i].details +
      "</td><td id=" +
      i +
      " >" +
      final_data[i].price +
      "</td>" +
      "<td>" +
      "<Button id=" +
      final_data[i].id +
      " class='btn' value=" +
      i +
      ">Add to wishlist</Button>" +
      "</td>" +
      "</tr>";
    $("#tabel").append(row);
  }
}

function harga() {
  let finalPrice = 0;
  let displayPrice = 0;
  $("button").click(function() {
    let currentPrice = finalPrice;
    let tempPrice = Number($("#" + $(this).attr("value")).text());
    if ($(this).text() === "Add to wishlist") {
      $(this).text("Remove from wishlist");
      displayPrice = currentPrice + tempPrice;
      finalPrice = currentPrice + tempPrice;
      $("#buy").text(displayPrice);
    } else {
      $(this).text("Add to wishlist");
      displayPrice = currentPrice - tempPrice;
      finalPrice = currentPrice - tempPrice;
      $("#buy").text(displayPrice);
    }
  });
}
