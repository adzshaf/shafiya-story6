var final_cost = 0;
var final_data = [];

$(document).ready(function() {
  $.ajax({
    url: "/list-json/",
    type: "GET",
    data: {},
    dataType: "json",
    success: function(data) {
      let header =
        "<tr><th>Nomor</th><th>Id</th><th>Name</th><th>Details</th><th>Price</th><th>Mau beli</th></tr><div id='row'></div>";
      $("#tabel").append(header);

      final_data = Array.from(data);
      for (var i = 0; i < final_data.length; ++i) {
        var row =
          "<tr>\
        <td>" +
          (i + 1) +
          "</td><td>" +
          final_data[i].id +
          "</td><td>" +
          final_data[i].name +
          "</td><td>" +
          final_data[i].details +
          "</td><td id=" +
          i +
          " >" +
          final_data[i].price +
          "</td>" +
          "<td>" +
          "<Button id=" +
          final_data[i].id +
          " class='btn' value=" +
          i +
          ">Add to wishlist</Button>" +
          "</td>" +
          "</tr>";
        $("#tabel").append(row);
      }
      urusintombol();
    }
  });
});

function urusintombol() {
  let hargafinal = 0;
  let hargadisplay = 0;
  $("button").click(function() {
    // Function
    let harganow = hargafinal;
    let hargatemp = Number($("#" + $(this).attr("value")).text());
    if ($(this).text() === "Add to wishlist") {
      $(this).text("Remove from wishlist");
      hargadisplay = rupiah(harganow + hargatemp);
      hargafinal = harganow + hargatemp;
      $("#tobuy").text(hargadisplay);
      $(this).addClass("red");
      console.log("masuk satu");
      console.log("harganow " + harganow);
      console.log("Hargatemp " + hargatemp);
      console.log("tobuy " + $("#tobuy").text());
      console.log("Hargafinal " + hargafinal);
    } else {
      $(this).text("Add to wishlist");
      hargadisplay = rupiah(harganow - hargatemp);
      hargafinal = harganow - hargatemp;
      $("#tobuy").text(hargadisplay);
      $(this).removeClass("red");
      console.log("masuk dua");
    }
  });
}

function rupiah(money) {
  var myNumeral = numeral(money).format("0,0");
  return myNumeral;
}
