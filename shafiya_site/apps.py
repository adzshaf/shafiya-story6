from django.apps import AppConfig


class ShafiyaSiteConfig(AppConfig):
    name = 'shafiya_site'
