from django.test import TestCase
from django.http import HttpRequest
from django.urls import resolve
from django.test import Client

from .models import Message
from .forms import MessageForm
from django.utils import timezone

from . import views

import time 

from selenium import webdriver
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.urls import reverse
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

class SampleTest(TestCase):
    # test eksistensi landing page
    def test_homepage(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_home_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, views.home)

    def test_status_template_used(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'page/index.html')

    def test_dummypage(self):
        response = Client().get('/wek')
        self.assertEqual(response.status_code,404)

    def test_apakabar(self):
        response = Client().post('/', {})
        self.assertIn("Apa kabar?", response.content.decode())
    
    def test_create_new_model(self):
        new_message = Message.objects.create(waktu=timezone.now(), pesan ="Saya senang")

        counting_all_pesan = Message.objects.all().count()
        self.assertEqual(counting_all_pesan, 1)

    def test_form_validated(self):
        form = MessageForm(data={'pesan':'', 'waktu':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['your_message'],
            ['This field is required.']
        )

    def test_message_completed(self):
        test_str = 'Saya senang'
        response_post = Client().post('/', {'pesan':test_str,'waktu':timezone.now})
        self.assertEqual(response_post.status_code,200)
        response = Client().get('/')
        html_response = response.content.decode('UTF-8')

    def test_form(self):
        response = Client().post('/',{'pesan':'Saya senang'})
        self.assertIn("</form>",response.content.decode())

    def test_cekoutput(self):
        response = Client().post('/',{'pesan': 'Saya senang'})
        self.assertIn("Daftar Output",response.content.decode())

    # tes untuk halaman baru
    def test_newpage(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code,200)

    def test_profile_using_index_func(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, views.profile)

    def test_profile_template_used(self):
        response = Client().get('/profile/')
        self.assertTemplateUsed(response, 'page/profile.html')

    def test_nyari_nama(self):
        response = Client().get('/profile/')
        self.assertIn("Shafiya Ayu Adzhani",response.content.decode())
    
    def test_nyari_npm(self):
        response = Client().get('/profile/')
        self.assertIn("1806235845",response.content.decode())

    def test_img(self):
        response = Client().get('/profile/')
        self.assertIn("<img",response.content.decode())

class FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.selenium.get('http://localhost:8000')
    
    def tearDown(self):
        self.selenium.quit()

    def test_opening_a_browser(self):
        selenium = self.selenium
        self.assertIn('Shafiya\'s Story 6',selenium.title)
    
    def test_halo_apa_kabar_present(self):
        selenium = self.selenium
        apa = selenium.find_element_by_id("apakabar").text
        self.assertEquals(apa, "Apa kabar?")

    def test_inputting_to_input(self):
        selenium = self.selenium
        message = "Coba Coba"
        text_area = selenium.find_element_by_id("pesan")
        text_area.send_keys(message)
        sub_button = selenium.find_element_by_id("tombol")
        sub_button.click()
        self.assertIn(message,selenium.page_source)

    # test untuk story 8
    def test_ada_accordion_aktivitas(self):
        selenium = self.selenium
        a_href = selenium.find_element_by_id("profile")
        a_href.click()
        accordion = selenium.find_element_by_class_name("accordion-header").text
        self.assertEquals(accordion, "Aktivitas")

    def test_ada_button_tema(self):
        selenium = self.selenium
        a_href = selenium.find_element_by_id("profile")
        a_href.click()
        button_tema = selenium.find_element_by_id("switch").text
        self.assertEquals(button_tema, "Switch Theme")

    def test_ganti_tema(self):
        selenium = self.selenium
        a_href = selenium.find_element_by_id("profile")
        a_href.click()
        button_tema = selenium.find_element_by_id("switch")
        time.sleep(10)
        button_tema.click()
        bg = selenium.find_element_by_tag_name("body").value_of_css_property('background-color')
        self.assertIn('rgba(0, 0, 0, 1)', bg)

    # test untuk challenge
    def test_ada_h3(self):
        selenium = self.selenium
        h3 = selenium.find_element_by_id("daftar").text
        self.assertEquals(h3, "Daftar Output")
    
    def test_ahref_diklik(self):
        selenium = self.selenium
        a_href = selenium.find_element_by_id("profile")
        a_href.click()
        nama = selenium.find_element_by_id("nama").text
        self.assertEquals(nama, "Shafiya Ayu Adzhani")
    
    def test_contain_fontstyle(self):
        selenium = self.selenium
        fontstyle = selenium.find_element_by_id("apakabar").value_of_css_property('font-family')
        self.assertIn('Roboto', fontstyle)
    
    def test_backgroundcolor(self):
        selenium = self.selenium
        bg = selenium.find_element_by_class_name("dalam-border").value_of_css_property('background-color')
        self.assertIn('rgba(243, 186, 195, 1)', bg)

    # test challenge story8
    def test_loader(self):
        selenium = self.selenium
        a_href = selenium.find_element_by_id("profile")
        a_href.click()
        load = selenium.find_element_by_class_name("loader")
        self.assertTrue(load);