$(document).ready(function() {
  $("#subs-button")
    .closest("form")
    .submit(function() {
      return checkForm.apply($(this).find(":input")[0]);
    })
    .find(inputSelector)
    .keyup(checkForm)
    .keyup();

  $("#subsform").submit(function(e) {
    e.preventDefault();
    $.ajax({
      method: "POST",
      url: "/save_subscribers/",
      data: $("form").serialize(),
      success: function(status) {
        if (status.status == "True") {
          $("#buat-alert").html(
            "<div class='alert alert-success' role='alert'>Terima kasih sudah subscribe!</div>"
          );
        } else {
          $("#buat-alert").html(
            "<div class='alert alert-danger' role='alert'>Maaf, email sudah pernah digunakan sebelumnya!</div>"
          );
        }
      }
    });
    return false;
  });

  $("#unsubs-button").click(function() {});
  $("#button-subs").click(function() {
    $("#button-subs").attr("disabled", true);
    $.ajax({
      url: "/show_subscribers/",
      type: "GET",
      dataType: "json",
      success: function(data) {
        var row = "<tr>";
        for (var i = 0; i < data.subsdata.length; i++) {
          row += '<th scope ="col">' + (i + 1) + "</th>";
          row += '<td scope ="col">' + data.subsdata[i].name + "</td>";
          row += "<td>" + data.subsdata[i].email + "</td>";
          row +=
            "<td>" +
            '<button class="btn btn-default" id="unsubs-button" onClick="deleteSubs(' +
            data.subsdata[i].pk +
            ')"type="submit">Unsubscribe</button></td></tr>';
        }
        console.log(row);
        $("#subs-table").append(row);
      }
    });
  });
});

const inputSelector = ":input[Required]:visible";
function checkForm() {
  var isValidForm = true;
  $(this.form)
    .find(inputSelector)
    .each(function() {
      if (!this.value.trim()) {
        isValidForm = false;
      }
    });
  $(this.form)
    .find("#subs-button")
    .prop("disabled", !isValidForm);
  return isValidForm;
}

function deleteSubs(pk) {
  console.log(pk);
  $.ajax({
    method: "POST",
    url: "/delete_subscribers/",
    data: { pk: pk },
    success: function() {
      if (confirm("Apakah anda yakin untuk unsubscribe?")) {
        window.location.reload();
      } else {
        return false;
      }
    }
  });
}
